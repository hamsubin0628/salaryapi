package com.example.salaryapi.entity;

import com.example.salaryapi.enums.Position;
import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String employeeId;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Position position;

    @Column(nullable = false)
    private Double preTax;

    @Column(nullable = false)
    private Double nationalTax;

    @Column(nullable = false)
    private Double healthTax;

    @Column(nullable = false)
    private Double employeeTax;

    @Column(nullable = false)
    private Double careTax;

    @Column(nullable = false)
    private Double inComeTax;

    @Column(nullable = false)
    private Double taxFree;

    @Column(nullable = false)
    private Double afterTax;
}
