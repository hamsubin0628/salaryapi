package com.example.salaryapi.service;

import com.example.salaryapi.entity.Salary;
import com.example.salaryapi.model.SalaryItem;
import com.example.salaryapi.model.SalaryRequest;
import com.example.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalaryService {
    private final SalaryRepository salaryRepository;

    public void setSalary(SalaryRequest request){
        Salary addData = new Salary();
        addData.setName(request.getName());
        addData.setEmployeeId(request.getEmployeeId());
        addData.setPosition(request.getPosition());
        addData.setPreTax(request.getPreTax());
        addData.setNationalTax(request.getNationalTax());
        addData.setHealthTax(request.getHealthTax());
        addData.setEmployeeTax(request.getEmployeeTax());
        addData.setCareTax(request.getCareTax());
        addData.setInComeTax(request.getInComeTax());
        addData.setTaxFree(request.getTaxFree());
        addData.setAfterTax(request.getAfterTax());

        salaryRepository.save(addData);
    }

    public List<SalaryItem> getSalaries(){
        List<Salary> originList = salaryRepository.findAll();

        List<SalaryItem> result = new LinkedList<>();

        for (Salary salary : originList){
            SalaryItem addItem = new SalaryItem();
            addItem.setId(salary.getId());
            addItem.setEmployeeId(salary.getEmployeeId());
            addItem.setName(salary.getName());
            addItem.setPosition(salary.getPosition().getPositionEnum());
            addItem.setPreTax(salary.getPreTax());
            addItem.setTaxFree(salary.getTaxFree());
            addItem.setAfterTax(salary.getAfterTax());

            result.add(addItem);
        }
        return result;
    }
}
