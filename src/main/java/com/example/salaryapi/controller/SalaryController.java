package com.example.salaryapi.controller;

import com.example.salaryapi.model.SalaryItem;
import com.example.salaryapi.model.SalaryRequest;
import com.example.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryController {
    private final SalaryService salaryService;

    @PostMapping("/info")
    private String setSalary(@RequestBody SalaryRequest request){
        salaryService.setSalary(request);

        return "OK";
    }

    @GetMapping("/all")
    private List<SalaryItem> getSalaries(){
        return salaryService.getSalaries();
    }
}
