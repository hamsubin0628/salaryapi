package com.example.salaryapi.model;

import com.example.salaryapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryItem {
    private Long id;
    private String employeeId;
    private String name;
    private String  position;
    private Double preTax;
    private Double taxFree;
    private Double afterTax;
}
