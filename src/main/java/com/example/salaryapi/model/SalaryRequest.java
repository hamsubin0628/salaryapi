package com.example.salaryapi.model;

import com.example.salaryapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryRequest {
    private String employeeId;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Position position;

    private Double preTax;

    private Double nationalTax;

    private Double healthTax;

    private Double employeeTax;

    private Double careTax;

    private Double inComeTax;

    private Double taxFree;

    private Double afterTax;
}
